using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace UnilecsTests
{
    public class Task171
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Task_171()
        {
            Console.WriteLine(GetRepresentation(Int32.MaxValue));
            Console.WriteLine(GetRepresentation(1000000001));
            Console.WriteLine(GetRepresentation(1000000000));
            Console.WriteLine(GetRepresentation(999999999));
            Console.WriteLine(GetRepresentation(1000001));
            Console.WriteLine(GetRepresentation(1000000));
            Console.WriteLine(GetRepresentation(999999));
            Console.WriteLine(GetRepresentation(1001));
            Console.WriteLine(GetRepresentation(1000));
            Console.WriteLine(GetRepresentation(999));
            Console.WriteLine(GetRepresentation(551));
            Console.WriteLine(GetRepresentation(550));
            Console.WriteLine(GetRepresentation(549));
            Console.WriteLine(GetRepresentation(501));
            Console.WriteLine(GetRepresentation(500));
            Console.WriteLine(GetRepresentation(499));
            Console.WriteLine(GetRepresentation(101));
            Console.WriteLine(GetRepresentation(100));
            Console.WriteLine(GetRepresentation(99));
            Console.WriteLine(GetRepresentation(51));
            Console.WriteLine(GetRepresentation(50));
            Console.WriteLine(GetRepresentation(49));
            Console.WriteLine(GetRepresentation(21));
            Console.WriteLine(GetRepresentation(20));
            Console.WriteLine(GetRepresentation(19));
            Console.WriteLine(GetRepresentation(11));
            Console.WriteLine(GetRepresentation(10));
            Console.WriteLine(GetRepresentation(9));
            Console.WriteLine(GetRepresentation(1));
            Console.WriteLine(GetRepresentation(0));
        }

        private Dictionary<int, string> _2021_Task_4_Data_Big =
            new Dictionary<int, string>()
            {
                        { 1000000000, "billion" },
                        { 1000000, "million" },
                        { 1000, "thousand" }
            };

        private Dictionary<int, string> _2021_Task_4_Data_Small =
            new Dictionary<int, string>()
            {
                        { 900, "nine hundred" },
                        { 800, "eight hundred" },
                        { 700, "seven hundred" },
                        { 600, "six hundred" },
                        { 500, "five hundred" },
                        { 400, "four hundred" },
                        { 300, "three hundred" },
                        { 200, "two hundred" },
                        { 100, "one hundred" },
                        { 90, "ninety" },
                        { 80, "eighty" },
                        { 70, "seventy" },
                        { 60, "sixty" },
                        { 50, "fifty" },
                        { 40, "forty" },
                        { 30, "thirty" },
                        { 20, "twenty" },
                        { 19, "nineteen" },
                        { 18, "eighteen" },
                        { 17, "seventeen" },
                        { 16, "sixteen" },
                        { 15, "fifteen" },
                        { 14, "fourteen" },
                        { 13, "thirteen" },
                        { 12, "twelve" },
                        { 11, "eleven" },
                        { 10, "ten" },
                        { 9, "nine" },
                        { 8, "eight" },
                        { 7, "seven" },
                        { 6, "six" },
                        { 5, "five" },
                        { 4, "four" },
                        { 3, "three" },
                        { 2, "two" },
                        { 1, "one" }
            };

        private string GetRepresentation(Int32 number)
        {
            if (number < 0)
            {
                return "";
            }

            if (number == 0)
            {
                return "zero";
            }

            string result = "";
            int remain = number;

            //Get big numbers representation
            foreach (var item in _2021_Task_4_Data_Big)
            {
                remain = GetRepresentationBig(remain, item, ref result);
            }

            //Get representation of numbers less than 1000
            string remainStr = GetRepresentationSmall(remain);
            if (!String.IsNullOrEmpty(remainStr))
            {
                result += String.IsNullOrEmpty(result) ? "" : " ";
                result += remainStr;
            }

            return result;
        }

        //Representation of big numbers
        private Int32 GetRepresentationBig(Int32 number, KeyValuePair<int, string> data, ref string result)
        {
            Int32 remain = number;

            int cnt = 0;
            while (data.Key <= remain)
            {
                cnt++;
                remain -= data.Key;
            }

            if (cnt > 0)
            {
                result += String.IsNullOrEmpty(result) ? "" : " ";
                //Get representation of count of big number
                result += GetRepresentationSmall(cnt);
                //Add representation big number
                result += " " + data.Value;
            }

            return remain;
        }

        //Representation of numbers less than 1000
        private string GetRepresentationSmall(Int32 number)
        {
            string result = "";

            while (number > 0)
            {
                foreach (var item in _2021_Task_4_Data_Small)
                {
                    if (item.Key <= number)
                    {
                        number -= item.Key;
                        result += String.IsNullOrEmpty(result) ? "" : " ";
                        result += item.Value;
                    }
                }
            }

            return result;
        }
    }
}