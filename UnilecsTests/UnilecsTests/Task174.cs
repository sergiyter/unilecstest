﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;

namespace UnilecsTests
{
    public class Task174
    {
        [SetUp]
        public void Setup()
        {

        }

        private string GetData(int length)
        {
            StringBuilder result = new StringBuilder();
            int minLength = Words.Min(x => x.Letters.Length);
            Random rnd = new Random();

            StringBuilder buffer = new StringBuilder();

            if (length >= minLength)
            {
                while (buffer.Length <= length)
                {
                    int index = rnd.Next(0, Words.Length);
                    buffer.Append(Words[index].Letters);
                }
            }

            while (buffer.Length > 0)
            {
                int index = rnd.Next(0, buffer.Length);
                result.Append(buffer[index]);
                buffer.Remove(index, 1);
            }

            return result.ToString();
        }

        //Массив содержит слова с очередностью поиска и буквой-маркером
        Word[] Words = new Word[]
        {
            new Word("zero", 'z', 0, 0),
            new Word("one", 'o', 1 , 9),
            new Word("two", 'w', 2, 1),
            new Word("three", 't', 3, 7),
            new Word("four", 'u', 4, 2),
            new Word("five", 'f', 5, 5),
            new Word("six", 'x', 6, 3),
            new Word("seven", 's', 7 , 8),
            new Word("eight", 'g', 8, 4),
            new Word("nine", 'i', 9, 6),
        };

        [Test]
        public void Task_174()
        {
            string result = Parse("owoztneoer");
            result = Parse("fviefuro");

            string data = GetData(1000);
            result = Parse(data);

            Console.WriteLine(result);
        }

        string Parse(string data) 
        {
            string result = "";
            List<int> numbers = new List<int>();

            //строка-буффер
            string buffer = new string(data);
            //отсортированные по порядку обработки слова
            List<Word> words = Words.OrderBy(x => x.Order).ToList();
            //номера букв из уже найденных слов для удаления
            List<int> lettersToRemove = new List<int>();

            foreach (Word curWord in words)
            {
                //получить кол-во букв-маркеров данного слова в строке
                int curMarkerCount = 0;
                for(int i = 0; i < buffer.Length; i++)
                {
                    if (buffer[i] == curWord.Marker) { 
                        curMarkerCount++; 
                        //добавляем число в результат
                        numbers.Add(curWord.Number); 
                    }
                }

                if (curMarkerCount > 0)
                {
                    //массив с кол-вом найденных букв (чтобы не перебрать по кол-ву)
                    int[] cntLetters = new int[curWord.Letters.Length];
                    for (int i = 0; i < cntLetters.Length; i++)
                    {
                        cntLetters[i] = 0;
                    }

                    //ищем нужные буквы но не более нужного кол-ва
                    for (int i = 0; i < buffer.Length; i++)
                    {
                        for (int j = 0; j < cntLetters.Length; j++)
                        {
                            if ((cntLetters[j] < curMarkerCount) && curWord.Letters[j] == buffer[i])
                            {
                                cntLetters[j]++;
                                lettersToRemove.Add(i);
                            }
                        }
                    }

                    string newBuffer = "";
                    //создаём новую строку-буффер без уже использованных букв
                    for (int i = 0; i < buffer.Length; i++)
                    {
                        if (!lettersToRemove.Contains(i))
                        {
                            newBuffer += buffer[i];
                        }
                    }
                    buffer = newBuffer;
                    lettersToRemove = new List<int>();
                }
            }
            //сортируем и готовим результат строку
            numbers.Sort();
            foreach (var item in numbers)
            {
                result += item.ToString();
            }

            return result;
        }

        private class Word 
        { 
            //число прописью
            public string Letters { get; private set; }
            //буква-маркер
            public char Marker { get; private set; }
            //число
            public int Number { get; private set; }
            //порядок обработки
            public int Order { get; private set; }

            public Word(string letters, char letter, int number, int order)
            {
                this.Letters = letters;
                this.Marker = letter;
                this.Number = number;
                this.Order = order;
            }
        }

    }
}
