﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace UnilecsTests
{
    public class Task172
    {
        [SetUp]
        public void Setup()
        {

        }

        //размер стороны доски
        private sbyte Size { get; set; }

        //Содержит уже рассмотренные варианты досок
        private Dictionary<byte, List<Desk>> Processed = new Dictionary<byte, List<Desk>>();

        [Test]
        public void Task_172()
        {
            int result = 0;

            Size = 1;
            result = Check();
            Console.WriteLine($"{Size} - {result}");

            Size = 2;
            result = Check();
            Console.WriteLine($"{Size} - {result}");

            Size = 3;
            result = Check();
            Console.WriteLine($"{Size} - {result}");

            Size = 4;
            result = Check();
            Console.WriteLine($"{Size} - {result}");

            Size = 5;
            result = Check();
            Console.WriteLine($"{Size} - {result}");

            Size = 6;
            result = Check();
            Console.WriteLine($"{Size} - {result}");

            Size = 7;
            result = Check();
            Console.WriteLine($"{Size} - {result}");

            Size = 8;
            result = Check();
            Console.WriteLine($"{Size} - {result}");

            Size = 9;
            result = Check();
            Console.WriteLine($"{Size} - {result}");

        }

        //Создаёт начальный набор досок с одним ферзем из всех возможных комбинаций
        //Запускает рекурсивно функцию добавления ферзей
        private int Check() 
        {
            List<Desk> validDesks = new List<Desk>();
            //Инициализация коллекции обработанных вариантов
            Processed = new Dictionary<byte, List<Desk>>();
            for (byte i = 0; i < Size; i++) 
            { 
                Processed.Add((byte)(i + 1), new List<Desk>()); 
            }

            //Начальная расстановка по 1 ферзю в каждую клетку
            for (byte i = 0; i < Size; i++)
            {
                for (byte j = 0; j < Size; j++)
                {
                    Desk newDesk = new Desk(Size);
                    newDesk.PutQueen(i, j);

                    if (Size == 1 && newDesk.Success)
                    {
                        validDesks.Add(newDesk);
                    }

                    //Рекурсивное добавление ферзей
                    AddQueens(newDesk, validDesks, 1, Size);
                }
            }
            int result = validDesks.Count();
            return result;
        }

        //Рекурсивное добавление ферзей
        //На каждой итерации добавляем по ферзю
        private void AddQueens(Desk desk, List<Desk> result, byte recursion, int maxRecursion) 
        {
            //Не рассматриваем уже рассмотренные варианты
            List<Desk> processed = Processed[recursion];
            if (processed.Any(x=>x.Equals(desk)))
            {
                return;
            }
            processed.Add(desk);

            Desk newDesk = null;

            for (byte i = 0; i < Size; i++)
            {
                for (byte j = 0; j < Size; j++)
                {
                    newDesk = (Desk)desk.Clone();
                    bool curQueenResult = newDesk.PutQueen(i, j);

                    if (curQueenResult)
                    {
                        if (recursion + 1 < maxRecursion)
                        {
                            //Если количество расставленных ферзей меньше необходимого то продолжаем
                            AddQueens(newDesk, result, (byte)(recursion + 1), maxRecursion);
                        }
                        else
                        {
                            //проверка на то есть ли в результате такая же расстановка
                            if (newDesk.Success && result.FirstOrDefault(x=>x.Equals(newDesk))==null)
                            {
                                result.Add(newDesk);
                            }
                        }
                    }
                }
            }
        }

        //Класс доска - содержит данные по конкретной расстановке
        public class Desk : IEquatable<Desk>, ICloneable
        {
            //размер стороны доски
            public sbyte Size { get; private set; }
            
            //данные о фигурах на доске
            public sbyte[,] Data { get; private set; }

            //конструктор
            public Desk(sbyte size)
            {
                this.Size = size;
                Data = new sbyte[size, size];
            }

            //проверка на то стоит ли требуемое кол-во ферзей на доске
            public bool Success { get {
                    byte cnt = 0;
                    for (byte i = 0; i < Size; i++)
                    {
                        for (byte j = 0; j < Size; j++)
                        {
                            if (Data[i,j] == 1)
                            {
                                cnt++;
                            }
                        }
                    }
                    bool result = cnt == Size;
                    return result;
                } 
            }

            //Попытаться поставить ферзя на доску. Если позиция подходит то ставим ферзя и возвращает true
            public bool PutQueen(byte vertical, byte horizontal) 
            {
                //Проверка позиции 
                bool result = CheckPosition(vertical, horizontal);

                if (result)
                {
                    //Если подходит то записываем в массив 1
                    Data[vertical, horizontal] = 1;
                    //Забиваем все позиции по горизонталям, вертикалям и диагоналям -1 для ускорения дальнейших подсчетов
                    FillWrongPositions(vertical, horizontal);
                }

                return result;
            }

            //Проверка конкретной позиции на то можно ли поставить ферзя.
            //Если по горизонталям, вертикалям и диагоналям нет других ферзей возвращает true
            private bool CheckPosition(byte vertical, byte horizontal) 
            {
                bool result = Data[vertical, horizontal] == 0;

                //check vertical line
                if (result)
                {
                    for (int i = 0; i < Size; i++)
                    {
                        if (Data[vertical, i] == 1 && i != horizontal)
                        {
                            result = false;
                            break;
                        }
                    }
                }

                //check horizontal line
                if (result)
                {
                    for (int i = 0; i < Size; i++)
                    {
                        if (Data[i, horizontal] == 1 && i != vertical)
                        {
                            result = false;
                            break;
                        }
                    }
                }

                //check line to top left
                if (result)
                {
                    sbyte leftTopHorizontal = (sbyte)(horizontal - 1);
                    sbyte leftTopVertical = (sbyte)(vertical - 1);

                    while (leftTopHorizontal >= 0 && leftTopVertical >= 0)
                    {
                        if (Data[leftTopVertical, leftTopHorizontal] == 1)
                        {
                            result = false;
                            break;
                        }
                        leftTopHorizontal--;
                        leftTopVertical--;
                    }
                }                
                
                ////check line to bottom left
                //if (result)
                //{
                //    sbyte leftBottomHorizontal = (sbyte)(horizontal - 1);
                //    sbyte leftBottomVertical = (sbyte)(vertical + 1);

                //    while (leftBottomHorizontal >= 0 && leftBottomVertical < Size)
                //    {
                //        if (Data[leftBottomVertical, leftBottomHorizontal] == 1)
                //        {
                //            result = false;
                //            break;
                //        }
                //        leftBottomHorizontal--;
                //        leftBottomVertical++;
                //    }
                //}

                //check line to top right
                if (result)
                {
                    sbyte rightTopHorizontal = (sbyte)(horizontal + 1);
                    sbyte rightTopVertical = (sbyte)(vertical - 1);

                    while (rightTopHorizontal < Size && rightTopVertical >= 0)
                    {
                        if (Data[rightTopVertical, rightTopHorizontal] == 1)
                        {
                            result = false;
                            break;
                        }
                        rightTopHorizontal++;
                        rightTopVertical--;
                    }
                }

                ////check line to bottom right
                //if (result)
                //{
                //    sbyte rightBottomHorizontal = (sbyte)(horizontal + 1);
                //    sbyte rightBottomVertical = (sbyte)(vertical + 1);

                //    while (rightBottomHorizontal < Size && rightBottomVertical < Size)
                //    {
                //        if (Data[rightBottomVertical, rightBottomHorizontal] == 1)
                //        {
                //            result = false;
                //            break;
                //        }
                //        rightBottomHorizontal++;
                //        rightBottomVertical++;
                //    }
                //}

                return result;
            }

            //Заполняет все позиции по горизонталям, вертикалям и диагоналям вокруг ферзя -1 для ускорения подсчетов
            private void FillWrongPositions(byte vertical, byte horizontal)
            {
                //vertical line
                for (int i = 0; i < Size; i++)
                {
                    if (Data[vertical, i] == 0 && i != horizontal)
                    {
                        Data[vertical, i] = -1;
                    }
                }

                //horizontal line
                for (int i = 0; i < Size; i++)
                {
                    if (Data[i, horizontal] == 0 && i != vertical)
                    {
                        Data[i, horizontal] = -1;
                    }
                }

                //line to top left
                sbyte leftTopHorizontal = (sbyte)(horizontal - 1);
                sbyte leftTopVertical = (sbyte)(vertical - 1);

                while (leftTopHorizontal >= 0 && leftTopVertical >= 0)
                {
                    if (Data[leftTopVertical, leftTopHorizontal] == 0)
                    {
                        Data[leftTopVertical, leftTopHorizontal] = -1;
                    }
                    leftTopHorizontal--;
                    leftTopVertical--;
                }

                //line to bottom left
                sbyte leftBottomHorizontal = (sbyte)(horizontal - 1);
                sbyte leftBottomVertical = (sbyte)(vertical + 1);

                while (leftBottomHorizontal >= 0 && leftBottomVertical < Size)
                {
                    if (Data[leftBottomVertical, leftBottomHorizontal] == 0)
                    {
                        Data[leftBottomVertical, leftBottomHorizontal] = -1;
                    }
                    leftBottomHorizontal--;
                    leftBottomVertical++;
                }

                //line to top right
                sbyte rightTopHorizontal = (sbyte)(horizontal + 1);
                sbyte rightTopVertical = (sbyte)(vertical - 1);

                while (rightTopHorizontal < Size && rightTopVertical >= 0)
                {
                    if (Data[rightTopVertical, rightTopHorizontal] == 0)
                    {
                        Data[rightTopVertical, rightTopHorizontal] = -1;
                    }
                    rightTopHorizontal++;
                    rightTopVertical--;
                }

                //check line to top right
                sbyte rightBottomHorizontal = (sbyte)(horizontal + 1);
                sbyte rightBottomVertical = (sbyte)(vertical + 1);

                while (rightBottomHorizontal < Size && rightBottomVertical < Size)
                {
                    if (Data[rightBottomVertical, rightBottomHorizontal] == 0)
                    {
                        Data[rightBottomVertical, rightBottomHorizontal] = -1;
                    }
                    rightBottomHorizontal++;
                    rightBottomVertical++;
                }
            }

            //Сверяет 2 доски по расставленным ферзям
            public bool Equals([AllowNull] Desk other)
            {
                bool result = other != null;
                if (result)
                {
                    result = this.Size == other.Size;
                }
                if (result)
                {
                    for (byte i = 0; i < Size; i++)
                    {
                        for (byte j = 0; j < Size; j++)
                        {
                            if (Data[i, j] == 1 && Data[i, j] != other.Data[i, j])
                            {
                                result = false;
                                break;
                            }
                        }
                        if (!result)
                        {
                            break;
                        }
                    }
                }
                return result;
            }

            //Клонирование доски для дальнейших итераций
            public object Clone()
            {
                Desk result = new Desk(this.Size);
                for (byte i = 0; i < Size; i++)
                {
                    for (byte j = 0; j < Size; j++)
                    {
                        result.Data[i, j] = this.Data[i, j];
                    }
                }
                return result;
            }
        }



        #region Solution
        [Test]
        public void Task_172_Solution()
        {
            Console.WriteLine("Is Ok = {0}", GetQueenCombintations(1) == 1);
            Console.WriteLine("Is Ok = {0}", GetQueenCombintations(2) == 0);
            Console.WriteLine("Is Ok = {0}", GetQueenCombintations(3) == 0);
            Console.WriteLine("Is Ok = {0}", GetQueenCombintations(4) == 2);
            Console.WriteLine("Is Ok = {0}", GetQueenCombintations(5) == 10);
            Console.WriteLine("Is Ok = {0}", GetQueenCombintations(6) == 4);
            Console.WriteLine("Is Ok = {0}", GetQueenCombintations(7) == 40);
            Console.WriteLine("Is Ok = {0}", GetQueenCombintations(8) == 92);
            Console.WriteLine("Is Ok = {0}", GetQueenCombintations(9) == 352);

        }

        public static int CombintationCount = -1;

        public class ChessTable
        {
            public int Row;
            public int Col;
            public ChessTable(int row, int col)
            {
                Row = row; Col = col;
            }
        }

        public static int GetQueenCombintations(int n)
        {
            CombintationCount = 0;
            var table = new ChessTable[n];

            // Начинаем расставлять ферзей, начиная с 1й строки доски
            QueenCombintationsHelper(n, 0, table);

            return CombintationCount;
        }

        public static void QueenCombintationsHelper(int n, int row, ChessTable[] chessTable)
        {
            // Алгоритм расставил ферзей на всех строках таблицы
            if (row == n)
            {
                CombintationCount++;
                return;
            }

            // Будем проверять каждый столбец в текущей строке row
            for (int col = 0; col < n; col++)
            {
                // делаем предположение, что поле (row, col) является подходящим для ферзя
                // и далее проверяем это предположение
                var foundPosition = true;

                // проверяем все предыдущие строки до текущей row
                for (int r = 0; r < row; r++)
                {
                    // нет ли ферзей, стоящих в этом же столбце
                    var sameColumn = chessTable[r].Col == col;

                    // нет ли ферзей, стоящих на верхней левой диагонали от текущей позиции
                    var upLeftDiagonal = chessTable[r].Col - chessTable[r].Row == col - row;

                    // нет ли ферзей, стоящих на верхней правой диагонали от текущей позиции
                    var upRightDiagonal = chessTable[r].Col + chessTable[r].Row == col + row;

                    // Примечание: мы проверяем только "верхние" диагонали, т.е. те что идут вверх от текущей точки.
                    // "Нижние" диагонали проверять не надо, т.к. мы идем от верхней строки к нижней и очевидно,
                    // что ниже от текущей точки ферзей еще нет.

                    // если текущее поле не подходит, переходим к след.столбцу col
                    if (sameColumn || upLeftDiagonal || upRightDiagonal)
                    {
                        foundPosition = false;
                        break;
                    }
                }

                // если предположение было верным, ставим ферзя в поле (row, col)
                // и переходим к след.строке
                if (foundPosition)
                {
                    chessTable[row] = new ChessTable(row, col);
                    QueenCombintationsHelper(n, row + 1, chessTable);
                }
            }
        } 
        #endregion


    }
}
