﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace UnilecsTests
{
    public class Task173
    {
        [SetUp]
        public void Setup()
        {

        }

        //минимально возможное число в списке
        int Min { get; set; } = -100;
        //максимально возможное число в списке
        int Max { get; set; } = 100;

        /// <summary>
        /// Генерирует набор списков
        /// </summary>
        /// <param name="listsCount">кол-во списков в наборе</param>
        /// <param name="minLength">минимально возможная длина списка</param>
        /// <param name="maxLength">максимально возможная длина списка</param>
        /// <returns></returns>
        List<int>[] GetData(int listsCount, int minLength, int maxLength)
        {
            Random rnd = new Random();
            List<int>[] result = new List<int>[listsCount];

            for (int i = 0; i < listsCount; i++)
            {
                int elemCount = rnd.Next(minLength, maxLength + 1);
                List<int> curList = new List<int>();
                for (int j = 0; j < elemCount; j++)
                {
                    curList.Add(rnd.Next(Min, Max + 1));
                }
                curList.Sort();
                result[i] = curList;
            }

            return result;
        }

        //Проверка результата
        private bool Check(List<int>[] lists, List<int> combination)
        {
            bool result = true;

            int curComboElement = combination[0];
            int comboSum = curComboElement;

            //Проверка отсортированности
            for (int i = 1; i < combination.Count; i++)
            {
                if (combination[i] < curComboElement)
                {
                    result = false;
                    break;
                }
                curComboElement = combination[i];
                comboSum += curComboElement;
            }

            //Проверка наличия всех элементов через суммирование
            if (result)
            {
                int listsSum = 0;
                for (int i = 0; i < lists.Length; i++)
                {
                    listsSum += lists[i].Sum();
                }
                result = listsSum == comboSum;
            }

            return result;
        }


        [Test]
        public void Task_173()
        {
            List<int>[] lists = GetData(1, 1, 1);
            List<int> combination = Combine(lists);
            Assert.IsTrue(Check(lists, combination));

            //погоняем тест
            for (int i = 0; i < 100000; i++)
            {
                lists = GetData(10, 10, 1000);
                combination = Combine(lists);
                Assert.IsTrue(Check(lists, combination));
            }
            
        }

        //соединяет набор списков в один
        private List<int> Combine(List<int>[] lists) 
        {
            List<int> result = new List<int>();

            Buffer buffer = new Buffer(lists);
            int? curInt = 0;
            while (curInt != null)
            {
                curInt = buffer.Next();
                if (curInt!=null)
                {
                    result.Add(curInt??0);
                }
            }

            return result;
        }

        //класс возвращающий следующее минимальное число
        private class Buffer 
        { 
            private List<int>.Enumerator[] Lists { get; set; }

            //буффер минимальных значений из каждого списка
            private int?[] BufferValues { get; set; }

            public Buffer(List<int>[] lists)
            {
                Lists = new List<int>.Enumerator[lists.Length];
                BufferValues = new int?[lists.Length];
                for (int i = 0; i < lists.Length; i++)
                {
                    Lists[i] = lists[i].GetEnumerator();
                    Lists[i].MoveNext();
                    BufferValues[i] = this.Lists[i].Current;
                }
            }

            //Возвращает следующее минимальное число из буфера
            public int? Next() 
            {
                int? result = null;

                int? curMin = BufferValues.Min();
                
                if (curMin != null)
                {
                    result = curMin ?? 0;
                    for (int i = 0; i < Lists.Length; i++)
                    {
                        if (BufferValues[i] == curMin)
                        {
                            if (Lists[i].MoveNext())
                            {
                                BufferValues[i] = Lists[i].Current;
                            }
                            else
                            {
                                BufferValues[i] = null;
                            }
                            break;
                        }
                    }
                }

                return result;
            }
        }

    }
}
